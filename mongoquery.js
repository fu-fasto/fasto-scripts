db.Store.aggregate([
  {
    $group: {
      _id: {
        id: "$city_id",
        name: "$city",
      },
      count: { $sum: 1 },
    },
  },
  { $project: { id: "$_id.id", name: "$_id.name", count: 1 } },
  { $sort: { count: -1 } },
]);

db.Store.aggregate([
  {
    $group: {
      _id: {
        id: "$district_id",
        name: "$district",
      },
      count: { $sum: 1 },
    },
  },
  { $sort: { count: -1 } },
]);

db.Store.aggregate([
  {
    $group: {
      _id: {
        id: "$cuisines._id",
        name: "$cuisines.name",
      },
      count: { $sum: 1 },
    },
  },
  { $sort: { count: -1 } },
]);

db.Store.aggregate([
  { $project: { _id: 0, cuisines: 1 } },
  { $unwind: "$cuisines" },
  { $group: { _id: "$cuisines", count: { $sum: 1 } } },
  { $project: { _id: 0, id: "$_id._id", name: "$_id.name", count: 1 } },
  { $sort: { count: -1 } },
]);

db.Store.aggregate([
  {
    $search: {
      text: {
        path: "name",
        query: "naw yark",
        fuzzy: {
          maxEdits: 1,
          maxExpansions: 100,
        },
      },
    },
  },
]);
